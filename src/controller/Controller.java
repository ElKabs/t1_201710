package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.data_structures.NumbersBag;
import model.logic.IntegersBagOperations;
import model.logic.NumbersBagOperations;

public class Controller<N extends Number> {

  private static NumbersBagOperations model = new NumbersBagOperations();
  
  
  public static NumbersBag createBag(ArrayList<Number> values){
         return new <Number> NumbersBag(values);    
  }
  
  
  public static Number getMean(NumbersBag bag){
    return model.computeMean(bag);
  }
  
  public static Number getMax(NumbersBag bag){
    return model.getMax(bag);
  }
  
  public static Number getMin(NumbersBag bag){
    return model.getMin(bag);
  }
  
  public static ArrayList getEvens(NumbersBag bag){
    return model.getEvens(bag);
  }
  
  public static ArrayList getPrimos(NumbersBag bag){
    return model.getPrimos(bag);
  }
  
}
