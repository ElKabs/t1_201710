package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;
import model.data_structures.NumbersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(NumbersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(NumbersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getMin(NumbersBag bag)
	{
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if(min > value)
				{
					min = value;
				}
			}
		}
		return min;
	}
	
	public int getEvens(NumbersBag bag)
	{
		int evens = 0;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				int value = iter.next();
				if(value%2==0)
				{
					evens++;
				}
			}
		}
		return evens;
	}
	
	public int getPrimos(NumbersBag bag)
	{
		int primos = 0;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				int value = iter.next();
				int a = 0;
				for(int i=1; i<(value+1); i++)
				{
					if(value%i==0)
					{
						a++;
					}
				}
				if(a==2)
				{
					primos++;
				}
				
			}
		}
		return primos;
	}
}
