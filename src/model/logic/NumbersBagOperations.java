package model.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

import model.data_structures.IntegersBag;
import model.data_structures.NumbersBag;
import model.data_structures.NumbersBag;

public class NumbersBagOperations {
  
  public Number computeMean(NumbersBag <Double> bag){
    Double mean = (double) 0;
    int length = 0;
    if(bag != null){
      Iterator<Double> iter = bag.getIterator();
      while(iter.hasNext()){
        mean += iter.next();
        length++;
      }
      if( length > 0) mean = mean / length;
    }
    return mean;
  }
  
  
  public Number getMax(NumbersBag <Double> bag){
    Double max = Double.MIN_VALUE;
      Double value;
    if(bag != null){
      Iterator<Double> iter = bag.getIterator();
      while(iter.hasNext()){
        value = iter.next();
        if( max < value){
          max = value;
        }
      }
      
    }
    return max;
  }
  
  public Number getMin(NumbersBag <Double> bag)
  {
    Double min = Double.MAX_VALUE;
    Double value;
    if(bag != null)
    {
      Iterator<Double> iter = bag.getIterator();
      while(iter.hasNext())
      {
        value = iter.next();
        if(min > value)
        {
          min = value;
        }
      }
    }
    return min;
  }
  
  public ArrayList getEvens(NumbersBag <Double> bag)
  {
    Double evens = (double) 0;
    ArrayList evensA = new ArrayList();
    if(bag != null)
    {
      Iterator<Double> iter = bag.getIterator();
      while(iter.hasNext())
      {
        Double value = iter.next();
        if(value%2==0)
        {
        	evens++;
        	evensA.add(value);
        }
        
      }
    }
    return evensA;
  }
  
  public ArrayList getPrimos(NumbersBag <Double> bag)
  {
    Double primos = (double) 0;
    ArrayList primes = new ArrayList<>();
    if(bag != null)
    {
      Iterator<Double> iter = bag.getIterator();
      while(iter.hasNext())
      {
        Double value = iter.next();
        int a = 0;
        for(int i=1; i<(value+1); i++)
        {
          if(value%i==0)
          {
            a++;
          }
        }
        if(a==2)
        {
          primes.add(value);
          primos++;
        }
        
      }
    }
    return primes;
  }
}
